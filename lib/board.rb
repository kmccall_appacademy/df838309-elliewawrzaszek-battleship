class Board
  attr_reader :grid

DISPLAY = {
  nil => " ",
  :s => " ",
  :x => :x,
  :* => :*
}

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def self.random
    self.new(self.default_grid, true)
  end

  def initialize(grid = self.class.default_grid, random = false)
    @grid = grid
    randomize if random
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, value)
    row, col = pos
    grid[row][col] = value
  end

  def count
    grid.flatten.select{ |el| el == :s}.length
  end

  def empty?(pos = nil)
    if pos
      self[pos].nil?
    else
      return true unless @grid.flatten.any?{|space| space != nil}
    end
  end

  def full?
    @grid.flatten.none? {|space| space.nil?}
  end

  def place_random_ship
    raise "error" if full?
    pos = random_pos
    until empty?(pos)
      pos = random_pos
    end
    self[pos] = :s
  end

  def random_pos
    [rand(size), rand(size)]
  end

  def size
    grid.length
  end

  def randomize(count = 10)
    count.times {place_random_ship}
  end

  def won?
    @grid.flatten.none? {|pos| pos == :s}
  end

  def in_range?(pos)
    pos.all? { |x| x.between?(0, grid.length - 1)}
  end

  def display
    x_axis = (0..9).to_a.join(" ")
    p "  #{x_axis}"
    grid.each_with_index{|row, i| display_row(row, i)}
  end

  def display_row(row, i)
    chars = row.map{|el| DISPLAY[el]}.join(" ")
    p "#{i} #{chars}"
  end

end
