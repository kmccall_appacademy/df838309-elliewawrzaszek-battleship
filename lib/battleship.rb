require_relative "player"
require_relative "board"

class BattleshipGame
  attr_reader :player, :board

  def initialize(player = HumanPlayer.new("Ellie"), board = Board.random)
    @player = player
    @board = board
    @hit = false
  end

  def hit?
    @hit
  end

  def attack(pos)
    if board[pos] == :s
      @hit = true
      board[pos] = :*
    else
      @hit = false
      board[pos] = :x
    end

  end

  def display_status
    system("clear")
    board.display
    if hit?
      puts "It's a hit!"
      puts "There are #{count} ships on the water"
    else
      puts "There are #{count} ships on the water"
      puts "Sink them before they sink you!"
    end
  end

  def play_turn
    pos = nil

    until valid_play?(pos)
      display_status
      pos = player.get_play
    end
    attack(pos)
  end

  def play
    puts " \n"
    puts "***** WELCOME TO BATTLESHIP! *****"
    play_turn until game_over?
  end

  def count
    board.count
  end

  def valid_play?(pos)
    pos.class == Array && board.in_range?(pos)
  end

  def game_over?
    return true if board.won?
  end

end

if __FILE__ == $PROGRAM_NAME
  BattleshipGame.new.play
end
